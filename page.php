<?php
get_header();
?>
<div class="container container-foto">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">	    
			<?php if (have_posts()):while (have_posts()):the_post(); ?>
					<?php the_content(); ?>	
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
