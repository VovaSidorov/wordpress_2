<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>SiteName</title>
		<?php wp_head(); ?>
		<title><?php bloginfo('name'); ?></title>
    </head>
    <body>
        <div class="container container-header">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#responsive-menu">
					<span class="sr-only button-icon">Открыть навигацию</span>
					<span class="icon-bar button-icon"></span>
					<span class="icon-bar button-icon"></span>
					<span class="icon-bar button-icon"></span>
				</button>				
				<a href="<?php echo home_url() ?>"><img src="<?php bloginfo('template_url') ?>/images/logo.png" alt=""></a>

			</div>        
			<div class="collapse navbar-collapse bold-href" id="responsive-menu">
				<div class="pull-right ulogin">
					<script src="//ulogin.ru/js/ulogin.js"></script><div id="uLogin_e0251d71" data-uloginid="e0251d71"></div>
				</div>
				<?php
				$defaults = array(
					'theme_location' => '',
					'menu' => '',
					'container' => '',
					'container_class' => '',
					'container_id' => '',
					'menu_class' => 'nav navbar-nav navbar-nav-1',
					'menu_id' => '',
					'echo' => true,
					'fallback_cb' => 'wp_page_menu',
					'before' => '',
					'after' => '',
					'link_before' => '<div class="dropdown-toggle js-activated otstup">',
					'link_after' => '</div>',
					'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
					'depth' => 0,
					'walker' => ''
				);

				wp_nav_menu($defaults);
				?>
				
			</div>
		</div>
		<div id="carousel" class="carousel slide container container-mobilephone">
			<div class="row">
				<?php $i = 1; ?>
				<div class="carousel-inner">
					<?php
					$args = array(
						'posts_per_page' => 5,
						'category_name' => 'Banner',
					);
					$posts_array = get_posts($args);
					if ($posts_array):foreach ($posts_array as $post) : setup_postdata($post);
							?>

							<div class="item <?php if ($i == 1) echo 'active'; ?>">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="col-md-6 hidden-sm hidden-xs container-mobilephone1">
		<?php the_post_thumbnail(); ?>
									</div>
									<div class="col-md-6 col-sm-12 col-xs-6 container-mobilephone2">

										<h2 class="text1"><?php the_title(); ?></h2>
										<div class="text2"><?php the_content(); ?></div>

										<div class="text3"><?php the_excerpt(); ?></div>

										<a href="<?php the_permalink(); ?>" class="button-style">
											MORE INFO
										</a>
									</div>
									<div class="carousel-caption">
										<!--					<h3>first slide</h3>
															    <p>Description of the first slide</p>-->
									</div>
								</div>
							</div>

							<?php
							$i++;
						endforeach;
					endif;
					?>

					<!--
								
					<!--arrow switch slides-->
					<a href="#carousel" class="left carousel-control" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left"></span>
					</a>
					<a href="#carousel" class="right carousel-control" data-slide="next">
						<span class="glyphicon glyphicon-chevron-right"></span>
					</a>
				</div>

			</div>
		</div>



