<?php 


/*** Flexi ***

Downloadstyles and scripts

***/
add_action('wp_enqueue_scripts', 'load_style_script');
function load_style_script(){
	wp_enqueue_script('jquery', get_template_directory_uri() . '/js/jquery-1.11.3.min.js' );
	wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.js' );
	wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.css' );
	wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.css' );
	wp_enqueue_style('style', get_template_directory_uri() . '/style.css' );			
}


/*** Flexi ***

Menu

***/


register_nav_menu('menu', 'Меню');


/*** Flexi ***

thumbnail support

***/

add_theme_support('post-thumbnails');


/*** Flexi ***

Sidebar

***/

register_sidebar(array(
	'name'=>'header	',
	'id'=>'sidebar',
	'description'=>'Здесь размешайте виджеты сайдбара',
	'before_widget'=>'<div class="new_sidebar text-inset1>',
	'after_widget'=>'</div>',

));
register_sidebar(array(
	'name'=>'Виджеты входа',
	'id'=>'footer',
	'description'=>'Здесь размешайте виджеты сайдбара',
	'before_widget'=>'<div class="new_cabinet text_cabinet>',
	'after_widget'=>'</div>',

));



add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );


/*** Flexi ***

Slider

***/

add_action ('init', 'slider');
function slider(){
	register_post_type('slider', array(
		'public'=>true,
		'supports'=>array('title','thumbnail'),
		'labels'=>array(
			'name'=>'Слайдер',
			'all_items'=>'Все слайды',
			'add_new'=>'Добавить новый',
			'add_new_item'=>'Добавление слайда'
	
			
		)
	));

}

/*** Flexi ***

Gets post cat slug and looks for single-[cat slug].php and applies it

***/

add_filter('single_template', create_function(
       '$the_template',
       'foreach( (array) get_the_category() as $cat ) {
        if ( file_exists(TEMPLATEPATH . "/single-{$cat->slug}.php") )
        return TEMPLATEPATH . "/single-{$cat->slug}.php"; }
    return $the_template;' )
);


add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );
add_theme_support( 'automatic-feed-links' );
?>