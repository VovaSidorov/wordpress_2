<?php get_header(); ?>

<div class="container container-foto">
	<div class="row">
		<div class="col-md-8 col-sm-12 col-xs-12">
			<?php if (have_posts()):while (have_posts()):the_post(); ?>
					<?php if (in_category('White_backgroud')) { ?>
						<div class="col-md-4  col-sm-3 ">
							<?php the_post_thumbnail(); ?>
						</div>
						<div class="col-md-8 col-sm-9">
							<h2 class="text-blue1"><?php the_title(); ?></h2>
							<p><?php the_content(); ?></p>
						<?php } elseif (in_category('Blue_background')){ ?>
							<div class="col-md-2  col-sm-3 ">
								<?php the_post_thumbnail(); ?>
							</div>
							<div class="col-md-10 col-sm-9">
								<h2 class="text-blue1"><?php the_title(); ?></h2>
								<p><?php the_content(); ?></p>
						<?php } else { ?>
								<div class="col-md-10 col-md-offset-1">               
								<h2 class="text-blue1"><?php the_title(); ?></h2>
								<p><?php the_content(); ?></p>
<?php } ?>
							<div class="comments">
								<?php comments_template(); ?>
								<?php
								comment_form(array(
									'comment_notes_after' => '',
									'comment_field' => '<p class="comment-form-comment"><textarea id="comment" name="comment" cols="45" rows="8" aria-required="false">' .
									'</textarea></p>'
								));
								?>
							</div>
						</div>
					<?php endwhile; ?>

				<?php else: ?>
					По запросу ничего не найдено
				<?php endif; ?>
			</div>	
			<div class="col-md-4 pading-1 col-sm-12 col-xs-12">
				<div class="tabs pading">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab-1" data-toggle="tab" class="text-inset">Eronped</a></li>
						<li><a href="#tab-2" data-toggle="tab">Centro</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane fade in active" id="tab-1">
							<div class="inset">
				
							    <?php
									if (is_active_sidebar('sidebar')) {
										(!dynamic_sidebar('sidebar'));
									} else {
										echo "Sidebar header is empty . Please add widgets";
									};
									?>
									 <?php
									if (is_active_sidebar('footer')) {
										(!dynamic_sidebar('footer'));
									} else {
										echo "Sidebar footer is empty . Please add widgets";
									};
									?>
								
								<p class="text-inset">Sedeglin libero commo</p>
								<p>Simasellus ultrices quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristiqu sem tris</p>
							</div>
							<div class="inset2">
								<p><a href="#" class="text-inset1">Cras ornare tristique elit sinto sum</a></p>
								<p><a href="#" class="text-inset1">Integet vitae libero ac risus egestas</a></p>
								<p><a href="#" class="text-inset1">Vestibulum commodo felis quis tortor</a></p>
								<p><a href="#" class="text-inset1">Donec qius diu dolor tempor interd</a></p>
								<p><a href="#" class="text-inset1">Fuscel obortis orem at ipsum semper</a></p>
							</div>
						</div>
						<div class="tab-pane fade" id="tab-2">
							<div class="inset">
								<p class="text-inset">Sedeglin libero commo</p>
								<p>Simasellus ultrices quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristiqu sem tris</p>
							</div>
							<div class="inset2">
								<p><a href="#" class="text-inset1">Cras ornare tristique elit sinto sum</a></p>
								<p><a href="#" class="text-inset1">Integet vitae libero ac risus egestas</a></p>
								<p><a href="#" class="text-inset1">Vestibulum commodo felis quis tortor</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php get_footer(); ?>