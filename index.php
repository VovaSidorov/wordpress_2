<?php get_header(); ?>
        <div class="container container-blue">
			
            <div class="row">	
				<?php 
				$args = array(
					'posts_per_page'   => 3,
					'category_name' => 'Blue_background',
				);
				$posts_array = get_posts($args);
				if ($posts_array):foreach ( $posts_array as $post ) : setup_postdata( $post ); ?>
				<?php if ( in_category('Blue_background') ) { ?>
                <div class="col-md-1 hidden-sm hidden-xs">
				
			     <?php the_post_thumbnail(); ?>
<!--             <img src="<?php bloginfo('template_url') ?>/images/flag.png" alt="">-->
                </div>
                <div class="col-md-3 col-sm-4 col-xs-4">
					  <h2 class="text-blue1"><?php the_title(); ?></h2>
                      <p><?php the_excerpt(); ?></p>
					  <p class="text-blue2"><a href="<?php the_permalink(); ?>" class="text-blue2-href arrow-right">Read More</a></p>
					   </div>
            <?php } ?>
					<?php endforeach;?>		
					<?php endif; ?>
            </div>
        </div>
        <div class="container container-foto">
            <div class="row">
                <div class="col-md-8 col-sm-7 col-xs-12">
						<?php if (have_posts() ):while (have_posts() ):the_post(); ?>
						<?php if ( in_category('White_backgroud') ) { ?>
					<div class="row">
                        <div class="col-md-3 indent-pictures col-sm-5 col-xs-3">
                                <?php the_post_thumbnail(); ?>
                        </div>
                        <div class="col-md-9 indent-text col-sm-7 col-xs-9">
                            <h2 class="title_text_foto"><?php the_title(); ?></h2>
                            <p class="text-foto"><?php the_excerpt(); ?></p>
                            <a href="<?php the_permalink(); ?>" class="button-style">
                                    MORE
                            </a>
                        </div>
						</div>
            <?php } ?>
					<?php endwhile; ?>				
					<?php endif; ?>
                </div>
                <div class="col-md-4 pading-1 col-sm-5 col-xs-8 col-xs-offset-2 col-sm-offset-0">

                     <div class="tabs pading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab-1" data-toggle="tab" class="text-inset">Eronped</a></li>
                            <li><a href="#tab-2" data-toggle="tab">Centro</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab-1">
                                <div class="inset">	
									
									
							      <?php
									if (is_active_sidebar('sidebar')) {
										(!dynamic_sidebar('sidebar'));
									} else {
										echo "Sidebar header is empty . Please add widgets";
									};
									?>
									 <?php
									if (is_active_sidebar('footer')) {
										(!dynamic_sidebar('footer'));
									} else {
										echo "Sidebar footer is empty . Please add widgets";
									};
									?>
									
						
                                    <p class="text-inset">Sedeglin libero commo</p>
                                    <p>Simasellus ultrices quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristiqu sem tris</p>	
                                </div>
                                <div class="inset2">
                                <p><a href="#" class="text-inset1">Cras ornare tristique elit sinto sum</a></p>
                                <p><a href="#" class="text-inset1">Integet vitae libero ac risus egestas</a></p>
                                <p><a href="#" class="text-inset1">Vestibulum commodo felis quis tortor</a></p>
                                <p><a href="#" class="text-inset1">Donec qius diu dolor tempor interd</a></p>
                                <p><a href="#" class="text-inset1">Fuscel obortis orem at ipsum semper</a></p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab-2">
                                <div class="inset">
                                    <p class="text-inset">Sedeglin libero commo</p>
                                    <p>Simasellus ultrices quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristiqu sem tris</p>
                                </div>
                                <div class="inset2">	
                                <p><a href="#" class="text-inset1">Cras ornare tristique elit sinto sum</a></p>
                                <p><a href="#" class="text-inset1">Integet vitae libero ac risus egestas</a></p>
                                <p><a href="#" class="text-inset1">Vestibulum commodo felis quis tortor</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container container-orange">
            <div class="row">
                <div class="col-md-12 ">
                    <p class="text-orange1">Praesent Clacerat</p>
                    <p class="text-orange2">Morbi interdum moilis sapien. Sed ac risus. Phasellus lacinia, manga a ullamocorper laoreet<br> lectus arcu pulvinar risus.</p>
                </div>
            </div>
        </div>
        <div class="container container-white">
            <div class="row">
                <div class="col-md-12 ">
                    <p class="text-white1">Vestibulum Commodo Felis</p>
                    <hr class="hr-width">
                    <p class="text-white2">Morbi interdum moilis sapien. Sed ac risus. Phasellus lacinia, manga </p>
                    <hr>
                </div>
            </div>
        </div>
        <div class="container container-calendar">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
					<?php if (have_posts() ):while (have_posts() ):the_post(); ?>
					<?php if ( in_category('Question_backgroud1') ) { ?>
                    <div class="col-md-2 col-sm-2 col-xs-2">	    
						<p class="number1"><?php the_time('d');?></p>
						<p class="number2"><?php the_time('M');?></p>
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-10">
                        <h2 class="text-calendar1"><?php the_title();?></h2>
                        <p class="text-calendar2"><?php comments_number('There are no comments', '1 comment', '% comments'); ?>
</a>
						</p>
                        <div class="text-calendar3"><?php the_excerpt(); ?><a href="<?php the_permalink(); ?>" class="arrow-right2">Read more</a></div>
                    </div>
<?php } else { ?>     
       <?php } ?>
					<?php endwhile; ?>
					
					<?php else: ?>
					
					<?php endif; ?>
				
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 container-calendar">
                    
					<?php
					$args = array(
						'number' => 2,
						'orderby' => 'comment_date',
						'order' => 'DESC',
						//'post_id' => 0,
						'type' => '', // только комментарии, без пингов и т.д...
					);

					if ($comments = get_comments($args)) {

						foreach ($comments as $comment) {
							?>
							<div class="col-md-12 question-border bg-message-right">
		                        <div class="col-md-9">
									<p class="text-question1">"<?php echo $comment->comment_content; ?>"</p>
									<p class="text-question2">- <?php echo $comment->comment_author; ?></p>
								</div>
		                    </div>
						<?php
						}
					}
					?>
                            
                </div>
            </div>
        </div> 
		
<?php get_footer(); ?>